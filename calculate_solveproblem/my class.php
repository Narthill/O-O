<meta charset="UTF-8">
<?php
/**1.一个球以V=1M/S的初速度匀速水平运动，求10s后球的位移
*2.一个球以V=1M/S的初速度，并以a=1m/s^2的加速度水平运动，求10s后球位移
*3.一个球初速度为0，以a1=3m/s^2的水平加速度与a2=4m/s^2的竖直向下的加速度运动，求10s后球的位移
*/
class SolveProblem
{
	public $itemname;
	public $v;
	public $t;
	public $x;
	public $a;
	function __construct($v1,$t1,$a1)
	{
		$this->v=$v1;
		$this->t=$t1;
		$this->a=$a1;
	}
	public function item()
	{
		$this->x=$this->v * $this->t+0.5 * $this->a * $this->t * $this->t;
		echo $this->x."米"."<br/>";
	}
}

$problem_1=new SolveProblem("1","10","0");
$problem_1->itemname='题目一：';
echo $problem_1->itemname;
//$problem_1->v="1";
//$problem_1->t="10";
//$problem_1->a="0";
$problem_1->item();


$problem_2=new SolveProblem("1","10","1");
$problem_2->itemname='题目二：';
echo $problem_2->itemname;
//$problem_2->v="1";
//$problem_2->t="10";
//$problem_2->a="1";
$problem_2->item();

$problem_3=new SolveProblem("0","10","5");
$problem_3->itemname='题目三：';
echo $problem_3->itemname;
//$problem_3->v="0";
//$problem_3->t="10";
//$problem_3->a="5";
$problem_3->item();
?>